import "./Home.scss";
import Header from "./Header/Header";
import { BrowserRouter as Router } from "react-router-dom";
import Banner from "./Banner/Banner";

function App() {
  return (
    <Router>
      <Header />
      <Banner />
    </Router>
  );
}

export default App;
