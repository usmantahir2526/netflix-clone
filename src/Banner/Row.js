const Card = ({ img }) => {
  return <img className="card" src={img} alt="cover" />;
};

const Row = ({ title, img = [] }) => {
  const img_url = "https://image.tmdb.org/t/p/w500";
  return (
    <div className="row">
      <h2>{title}</h2>
      <div>
        {img.map((ite, i) => (
          <Card key={i} img={`${img_url}${ite.poster_path}`} />
        ))}
      </div>
    </div>
  );
};

export default Row;
