import React, { useEffect, useState } from "react";
import Row from "./Row";
import axios from "axios";
import { BiPlay } from "react-icons/bi";
import { HiPlusSm } from "react-icons/hi";

const Banner = () => {
  const [newPopularMovies, setNewPopularMovies] = useState([]);
  const [topMovies, setTopRatedMovies] = useState([]);
  const img_url = "https://image.tmdb.org/t/p/w500";

  useEffect(() => {
    const popularMovies = async () => {
      const data = await axios.get(
        "https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=popularity.desc&total_pages=1",
        {
          headers: {
            Authorization:
              "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYWQyNmEwNzc0OTI5MDYyMGM1ZmE2YmY2NzJkMmNiYSIsInN1YiI6IjY1MTE4ODQxZTFmYWVkMDExZDVmM2EyOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.jn8zYHplqhewM3jVbe9hTPkPUZkuoNpkf-AzpUPlOoU",
            Accept: "application/json",
          },
        }
      );
      setNewPopularMovies(data.data.results);
    };

    const topRated = async () => {
      const data = await axios.get(
        "https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page=1&sort_by=vote_average.desc&without_genres=99,10755&total_pages=1",
        {
          headers: {
            Authorization:
              "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJjYWQyNmEwNzc0OTI5MDYyMGM1ZmE2YmY2NzJkMmNiYSIsInN1YiI6IjY1MTE4ODQxZTFmYWVkMDExZDVmM2EyOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.jn8zYHplqhewM3jVbe9hTPkPUZkuoNpkf-AzpUPlOoU",
            Accept: "application/json",
          },
        }
      );
      setTopRatedMovies(data.data.results);
    };
    popularMovies();
    topRated();

    // console.log("newww", newPopularMovies);
  }, [newPopularMovies, topMovies]);

  return (
    <section className="home">
      <div
        className="banner"
        style={{
          backgroundImage: `url(
            "https://static.toiimg.com/thumb/msid-67884746,width-1280,resizemode-4/67884746.jpg"
          )`,
        }}
      >
        <h1>Shrek's Yule Log</h1>
        <p>
          Inside the Shrek home, a fire blazes in the fireplace during a
          Christmas party. Christmas music plays in the background as Shrek, his
          family, and his friends, come and go While tending the fire and
          engaging in various Christmas activities.
        </p>

        <div>
          <button>
            Play <BiPlay />
          </button>
          <button>
            My List <HiPlusSm />
          </button>
        </div>
      </div>

      <Row title="Popular On Netflix" img={newPopularMovies} />
      <Row title="Top Rated Movies" img={topMovies} />
    </section>
  );
};

export default Banner;
